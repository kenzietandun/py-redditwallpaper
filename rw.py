#!/usr/bin/env python3

# this script combines image and quote to create a wallpaper

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
from io import BytesIO
from random import shuffle
import configparser
import praw
import requests
import textwrap
import os

#HOME = os.getenv('HOME')
HOME = '/home/kenzie/'
HERE = os.path.dirname(os.path.abspath(__file__))
IMAGEDIR = HOME + '/Pictures/'

def read_config(configfile):
    # config file location
    configfile = '{home}/.config/{cfg}'.format(home=HOME, cfg=configfile)

    cfg = configparser.ConfigParser()
    cfg.read(configfile)

    # reddit client id and secret
    r_clientid = cfg.get('reddit', 'client_id')
    r_clientsecret = cfg.get('reddit', 'client_secret')

    # wallpaper config
    w_subreddit = cfg.get('wallpaper', 'subreddit')
    w_sort = cfg.get('wallpaper', 'sort')
    w_range = cfg.get('wallpaper', 'range')

    # quote config
    q_subreddit = cfg.get('quote', 'subreddit')
    q_sort = cfg.get('quote', 'sort')
    q_range = cfg.get('quote', 'range')

    config = {
                 'r_clientid' : r_clientid,
                 'r_clientsecret' : r_clientsecret,
                 'w_subreddit' : w_subreddit,
                 'w_sort' : w_sort,
                 'w_range' : w_range,
                 'q_subreddit' : q_subreddit,
                 'q_sort' : q_sort,
                 'q_range' : q_range
             }

    return config

def init_reddit(config):
    redd = praw.Reddit(client_id = config['r_clientid'],
                       client_secret = config['r_clientsecret'],
                       user_agent = 'wallquotebot')

    return redd

def get_wallpaper(redd, config):
    subreddit = config['w_subreddit']
    sub_sort = config['w_sort']
    sub_range = config['w_range']
    if sub_sort == 'top':
        posts = redd.subreddit(subreddit).top(sub_range)
    elif sub_sort == 'hot':
        posts = redd.subreddit(subreddit).hot()

    posts = [ post for post in posts ] 
    posts = posts[0:20]
    shuffle(posts)
    for post in posts:
        # try to get image resolution
        try:
            data = requests.get(post.url).content
            im = Image.open(BytesIO(data))
            x, y = im.size
            if x >= 1920 and y >= 1080:
                ext = post.url.split('.')[-1]
                savedir = IMAGEDIR + 'wallpaper.' + ext
                im.save(savedir)
                break
        except Exception as e:
            print(e)

    return savedir

def get_quote(redd, config):
    subreddit = config['q_subreddit']
    sub_sort = config['q_sort']
    sub_range = config['q_range']
    if sub_sort == 'top':
        posts = redd.subreddit(subreddit).top(sub_range)
    elif sub_sort == 'hot':
        posts = redd.subreddit(subreddit).hot()
    
    posts = [ post for post in posts ]
    shuffle(posts)
    quote = posts[0].title
    author = posts[0].author

    return (str(quote), str(author))

def combine_wallpaper_quote(wallpaper, quote, author):
    import re
    quote = re.sub('[^A-Za-z0-9\s\.\,]+', '', quote)
    print(quote)
    
    im = Image.open(wallpaper)
    x, y = im.size

    max_x = 1920
    max_y = 1080
    max_ratio = max(max_x/x, max_y/y)
    im = im.resize((int(x*max_ratio), int(y*max_ratio)), Image.ANTIALIAS)
    # calculate new size
    x, y = im.size

    # get image colors in certain pixels
    image = im.load()
    pixelcount = 0
    r, g, b = 0, 0, 0
    for i in range(0, x, x//50):
        for j in range(0, y, y//50):
            pixelr, pixelg, pixelb = image[i,j]
            r += pixelr
            g += pixelg
            b += pixelb
            pixelcount += 1

    avg_color = ((r/pixelcount) + (g/pixelcount) + (b/pixelcount))//3
    if avg_color > 127:
        fontcolor = (0, 0, 0)
    else:
        fontcolor = (255, 255, 255)

    paragraph = textwrap.wrap(quote, width = 30)

    draw = ImageDraw.Draw(im)
    quotefont = HERE + '/fonts/BreeSerif-Regular.ttf'
    fontsize = 100
    font = ImageFont.truetype(quotefont, fontsize)

    w, h = draw.textsize(quote)

    lines = len(paragraph)
    middle_point = lines/2

    # start writing a bit above the middle point
    # so the text ends up in the centre of the image
    spacing = 120
    curr_h = (y-h)/2 - (middle_point*spacing)

    for line in paragraph:
        w, h = draw.textsize(line, font=font)
        #draw.text(((x-w)/2 - 1, curr_h), line, font=font, fill=(0,0,0,28))
        draw.text(((x-w)/2, curr_h), line, font=font, fill=fontcolor)
        curr_h += spacing

    curr_h += 70
    authorfont = HERE + '/fonts/BenchNine-Light.ttf'
    fontsize = 50
    font = ImageFont.truetype(authorfont, fontsize)
    author = '/u/' + author
    w, h = draw.textsize(author, font=font)
    draw.text(((x-w)/2, curr_h), author, font=font, fill=fontcolor)

    im.save(wallpaper)

def main():
    config = read_config('pyrw.ini')
    redd = init_reddit(config)
    wallpaper = get_wallpaper(redd, config)
    quote, author = get_quote(redd, config)
    combine_wallpaper_quote(wallpaper, quote, author)
    if os.path.isfile(HOME + '/Pictures/wallpaper.jpg'):
        os.system('DISPLAY=:0 /usr/bin/feh --bg-center ~/Pictures/wallpaper.jpg')
    else:
        os.system('DISPLAY=:0 /usr/bin/feh --bg-center ~/Pictures/wallpaper.png')

if __name__ == '__main__':
    main()
